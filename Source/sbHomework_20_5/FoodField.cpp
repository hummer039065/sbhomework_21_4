// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodField.h"
#include "Food.h"
#include "Math/UnrealMathUtility.h"


// Sets default values
AFoodField::AFoodField()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodField::BeginPlay()
{
	Super::BeginPlay();

	SetActorTickInterval(2);
	
}

// Called every frame
void AFoodField::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	SpawnFood();
	
}

void AFoodField::SpawnFood(int SpawnFood)
{
	RandomXCoord = FMath::RandRange(-1460.0f, 1550.0f);
	RandomYCoord = FMath::RandRange(-1460.0f, 1550.0f);

	for (int i = 0; i < SpawnFood; i++)
	{
		FVector SpawnLocation = FVector(RandomXCoord, RandomYCoord, 40);
		Food = GetWorld()->SpawnActor<AFood>(FoodClass, FTransform(FRotator(ForceInitToZero), SpawnLocation));
	}
}

