// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBaseElement.h"
#include "SnakeBase.h"

// Sets default values
ASnakeBaseElement::ASnakeBaseElement()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SnakeElementMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Snake Element"));
	SetRootComponent(SnakeElementMesh);
	SnakeElementMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SnakeElementMesh->SetCollisionResponseToAllChannels(ECR_Overlap);
	SnakeElementMesh->SetHiddenInGame(true);

}

// Called when the game starts or when spawned
void ASnakeBaseElement::BeginPlay()
{
	Super::BeginPlay();

	
	
}

// Called every frame
void ASnakeBaseElement::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeBaseElement::SetFirstElement_Implementation()
{
	SnakeElementMesh->OnComponentBeginOverlap.AddDynamic(this, &ASnakeBaseElement::HandleBeginOverlap);
}

void ASnakeBaseElement::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		ASnakeBase* Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->DestroySnake();
		}
	}
}

void ASnakeBaseElement::HandleBeginOverlap(
	UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherOverlappedComponent,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	if (IsValid(SnakeOwner))
	{
		SnakeOwner->SnakeElementOverlapped(this, OtherActor);
	}
	
}

void ASnakeBaseElement::ToggleCollision()
{
	if(SnakeElementMesh->GetCollisionEnabled() == ECollisionEnabled::QueryOnly)
	{
		SnakeElementMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
	else
	{
		SnakeElementMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	
}



