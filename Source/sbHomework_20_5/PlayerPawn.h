// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Camera/CameraComponent.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawn.generated.h"

class ASnakeBase;
class AFoodField;

UCLASS()
class SBHOMEWORK_20_5_API APlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawn();

	UPROPERTY()
	UCameraComponent* BaseCamera;

	UPROPERTY()
	ASnakeBase* SnakeBase;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeBaseClass;

	UPROPERTY(EditAnywhere)
	FVector InicialLocation = FVector(0,0,0);

	UPROPERTY()
	AFoodField* FoodField;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFoodField> FoodFieldClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void SpawnSnake();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	void HandleVerticalPlayerMove(float value);

	UFUNCTION()
	void HandleHorizontalPlayerMove(float value);

};
