// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "sbHomework_20_5GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SBHOMEWORK_20_5_API AsbHomework_20_5GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
