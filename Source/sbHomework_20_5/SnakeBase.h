// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeBaseElement;

UENUM()
enum class EMovingType : uint8
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SBHOMEWORK_20_5_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBaseElement> SnakeElementClass;

	UPROPERTY()
	ASnakeBaseElement* SnakeBaseElement;

	UPROPERTY()
	TArray<ASnakeBaseElement*> SnakeElements;

	UPROPERTY(EditAnywhere)
	float SnakeSize = 120.0f;

	UPROPERTY(EditAnywhere)
	float SnakeSpeed = 10.0f;

	UPROPERTY(EditAnywhere)
	EMovingType LastMoveDirection = EMovingType::DOWN;


	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SpawnSnakeElement(int SpawnElem = 1);
	
	void Move();

	void SnakeElementOverlapped(ASnakeBaseElement* OverlappedElement, AActor* OtherActor);

	void DestroySnake();

};
