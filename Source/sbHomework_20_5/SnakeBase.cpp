// Fill out your copyright notice in the Description page of Project Settings.

#include "SnakeBase.h"
#include "SnakeBaseElement.h"
#include "Interactable.h"


// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(SnakeSpeed);

	SpawnSnakeElement(1);
}

void ASnakeBase::SpawnSnakeElement(int SpawnElem)
{
	for (int i = 0; i < SpawnElem; i++)
	{
		FVector SpawnElementLocation = FVector(SnakeElements.Num() * SnakeSize, 0, -240);
		SnakeBaseElement = GetWorld()->SpawnActor<ASnakeBaseElement>(SnakeElementClass,FTransform(FRotator::ZeroRotator, SpawnElementLocation));
		SnakeBaseElement->SnakeOwner = this;
		int32 ElementIndex = SnakeElements.Add(SnakeBaseElement);

		if (ElementIndex == 0)
		{
			SnakeBaseElement->SetFirstElement();
			SnakeBaseElement->AddActorWorldOffset(FVector(0,0,280));
		}
	}
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	SetActorTickInterval(SnakeSpeed);

	Move();

}

void ASnakeBase::Move()
{
	FVector CurrentLocation(ForceInitToZero);
	
	switch (LastMoveDirection)
	{
	case EMovingType::UP:
		{
			CurrentLocation.X += SnakeSize;
		}
		break;
	case EMovingType::DOWN:
		{
			CurrentLocation.X -= SnakeSize;
		}
		break;
	case EMovingType::LEFT:
		{
			CurrentLocation.Y -= SnakeSize;
		}
		break;
	case EMovingType::RIGHT:
		{
			CurrentLocation.Y += SnakeSize;
		}
		break;
	default: ;
	}

	SnakeElements[0]->ToggleCollision();
	
	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		ASnakeBaseElement* LastElement = SnakeElements[i];
		ASnakeBaseElement* PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		LastElement->SetActorLocation(PrevLocation);
		LastElement->SnakeElementMesh->SetHiddenInGame(false);
	}

	SnakeElements[0]->AddActorWorldOffset(CurrentLocation);
	AddActorWorldOffset(CurrentLocation);
	SnakeElements[0]->ToggleCollision();
	SnakeElements[0]->SnakeElementMesh->SetHiddenInGame(false);
}

void ASnakeBase::SnakeElementOverlapped(ASnakeBaseElement* OverlappedElement, AActor* OtherActor)
{
	if (IsValid(OverlappedElement))
	{
		int32 IndexElem;
		SnakeElements.Find(OverlappedElement, IndexElem);
		bool bIsFirst = IndexElem == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(OtherActor);

		if(InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::DestroySnake()
{
	for (int i = 0; i <SnakeElements.Num(); i++)
	{
		SnakeElements[i]->Destroy();
	}
	Destroy();
}

