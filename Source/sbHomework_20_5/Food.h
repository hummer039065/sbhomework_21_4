// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "GameFramework/Actor.h"
#include "Food.generated.h"

class AFoodField;

UCLASS()
class SBHOMEWORK_20_5_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

	bool isBig;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	UFUNCTION(BlueprintNativeEvent)
	void SetBigFood();
	void SetBigFood_Implementation();

	UFUNCTION(BlueprintNativeEvent)
	void SetSmallFood();
	void SetSmallFood_Implementation();

};
