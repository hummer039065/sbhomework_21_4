// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerPawn.h"
#include "SnakeBase.h"
#include "FoodField.h"


// Sets default values
APlayerPawn::APlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BaseCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Base Camera"));
	SetRootComponent(BaseCamera);

}

// Called when the game starts or when spawned
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();

	SetActorRotation(FRotator(-90, 0, 0));

	SpawnSnake();
	
}

void APlayerPawn::SpawnSnake()
{
	FVector SpawnLocation = InicialLocation;
	FTransform SpawnSnakeBaseTransform = FTransform(FRotator::ZeroRotator, SpawnLocation);
	SnakeBase = GetWorld()->SpawnActor<ASnakeBase>(SnakeBaseClass, SpawnSnakeBaseTransform);
	FoodField = GetWorld()->SpawnActor<AFoodField>(FoodFieldClass, SpawnSnakeBaseTransform);
	
}

// Called every frame
void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical",this,&APlayerPawn::HandleVerticalPlayerMove);
	PlayerInputComponent->BindAxis("Horizontal",this,&APlayerPawn::HandleHorizontalPlayerMove);

}

void APlayerPawn::HandleVerticalPlayerMove(float value)
{
	if (IsValid(SnakeBase))
	{
		if (value > 0 && SnakeBase->LastMoveDirection != EMovingType::DOWN)
		{
			SnakeBase->LastMoveDirection = EMovingType::UP;
		}
		else if (value < 0 && SnakeBase->LastMoveDirection != EMovingType::UP)
		{
			SnakeBase->LastMoveDirection = EMovingType::DOWN;
		}
	}
	
}

void APlayerPawn::HandleHorizontalPlayerMove(float value)
{
	if (IsValid(SnakeBase))
	{
		if (value > 0 && SnakeBase->LastMoveDirection != EMovingType::RIGHT)
		{
			SnakeBase->LastMoveDirection = EMovingType::LEFT;
		}
		else if (value < 0 && SnakeBase->LastMoveDirection != EMovingType::LEFT)
		{
			SnakeBase->LastMoveDirection = EMovingType::RIGHT;
		}
	}
}

