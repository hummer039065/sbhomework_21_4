// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FoodField.generated.h"

class AFood;

UCLASS()
class SBHOMEWORK_20_5_API AFoodField : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFoodField();

	float RandomXCoord;

	float RandomYCoord;

	UPROPERTY()
	AFood* Food;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SpawnFood(int SpawnFood = 1);

};
