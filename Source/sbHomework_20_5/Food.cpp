// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	isBig = FMath::RandBool();

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();

	

}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	

	if (!isBig)
	{
		SetBigFood();	
	}
	else
	{
		SetSmallFood();
	}

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		ASnakeBase* Snake = Cast<ASnakeBase>(Interactor);
		
		if (IsValid(Snake))
		{
			if (!isBig)
			{
				Snake->SpawnSnakeElement(2);
			
				if (Snake->SnakeSpeed > 0.06)
				{
					Snake->SnakeSpeed -= 0.04;
				}
			}
			else
			{
				Snake->SpawnSnakeElement();
			
				if (Snake->SnakeSpeed > 0.06)
				{
					Snake->SnakeSpeed -= 0.02;
				}
			}
		}
	
		Destroy();
	}
}

void AFood::SetBigFood_Implementation()
{
	
}

void AFood::SetSmallFood_Implementation()
{
	
}
