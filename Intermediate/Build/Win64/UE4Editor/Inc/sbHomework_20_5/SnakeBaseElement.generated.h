// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef SBHOMEWORK_20_5_SnakeBaseElement_generated_h
#error "SnakeBaseElement.generated.h already included, missing '#pragma once' in SnakeBaseElement.h"
#endif
#define SBHOMEWORK_20_5_SnakeBaseElement_generated_h

#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_SPARSE_DATA
#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_RPC_WRAPPERS \
	virtual void SetFirstElement_Implementation(); \
 \
	DECLARE_FUNCTION(execToggleCollision); \
	DECLARE_FUNCTION(execHandleBeginOverlap); \
	DECLARE_FUNCTION(execSetFirstElement);


#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execToggleCollision); \
	DECLARE_FUNCTION(execHandleBeginOverlap); \
	DECLARE_FUNCTION(execSetFirstElement);


#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_EVENT_PARMS
#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_CALLBACK_WRAPPERS
#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeBaseElement(); \
	friend struct Z_Construct_UClass_ASnakeBaseElement_Statics; \
public: \
	DECLARE_CLASS(ASnakeBaseElement, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/sbHomework_20_5"), NO_API) \
	DECLARE_SERIALIZER(ASnakeBaseElement) \
	virtual UObject* _getUObject() const override { return const_cast<ASnakeBaseElement*>(this); }


#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnakeBaseElement(); \
	friend struct Z_Construct_UClass_ASnakeBaseElement_Statics; \
public: \
	DECLARE_CLASS(ASnakeBaseElement, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/sbHomework_20_5"), NO_API) \
	DECLARE_SERIALIZER(ASnakeBaseElement) \
	virtual UObject* _getUObject() const override { return const_cast<ASnakeBaseElement*>(this); }


#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeBaseElement(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeBaseElement) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeBaseElement); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeBaseElement); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeBaseElement(ASnakeBaseElement&&); \
	NO_API ASnakeBaseElement(const ASnakeBaseElement&); \
public:


#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeBaseElement(ASnakeBaseElement&&); \
	NO_API ASnakeBaseElement(const ASnakeBaseElement&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeBaseElement); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeBaseElement); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnakeBaseElement)


#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_PRIVATE_PROPERTY_OFFSET
#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_12_PROLOG \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_EVENT_PARMS


#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_PRIVATE_PROPERTY_OFFSET \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_SPARSE_DATA \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_RPC_WRAPPERS \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_CALLBACK_WRAPPERS \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_INCLASS \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_PRIVATE_PROPERTY_OFFSET \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_SPARSE_DATA \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_CALLBACK_WRAPPERS \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_INCLASS_NO_PURE_DECLS \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SBHOMEWORK_20_5_API UClass* StaticClass<class ASnakeBaseElement>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID sbHomework_20_5_Source_sbHomework_20_5_SnakeBaseElement_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
