// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SBHOMEWORK_20_5_SnakeBase_generated_h
#error "SnakeBase.generated.h already included, missing '#pragma once' in SnakeBase.h"
#endif
#define SBHOMEWORK_20_5_SnakeBase_generated_h

#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h_23_SPARSE_DATA
#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h_23_RPC_WRAPPERS
#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeBase(); \
	friend struct Z_Construct_UClass_ASnakeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/sbHomework_20_5"), NO_API) \
	DECLARE_SERIALIZER(ASnakeBase)


#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h_23_INCLASS \
private: \
	static void StaticRegisterNativesASnakeBase(); \
	friend struct Z_Construct_UClass_ASnakeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/sbHomework_20_5"), NO_API) \
	DECLARE_SERIALIZER(ASnakeBase)


#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeBase(ASnakeBase&&); \
	NO_API ASnakeBase(const ASnakeBase&); \
public:


#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeBase(ASnakeBase&&); \
	NO_API ASnakeBase(const ASnakeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnakeBase)


#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h_23_PRIVATE_PROPERTY_OFFSET
#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h_20_PROLOG
#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h_23_PRIVATE_PROPERTY_OFFSET \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h_23_SPARSE_DATA \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h_23_RPC_WRAPPERS \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h_23_INCLASS \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h_23_PRIVATE_PROPERTY_OFFSET \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h_23_SPARSE_DATA \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h_23_INCLASS_NO_PURE_DECLS \
	sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SBHOMEWORK_20_5_API UClass* StaticClass<class ASnakeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID sbHomework_20_5_Source_sbHomework_20_5_SnakeBase_h


#define FOREACH_ENUM_EMOVINGTYPE(op) \
	op(EMovingType::UP) \
	op(EMovingType::DOWN) \
	op(EMovingType::LEFT) \
	op(EMovingType::RIGHT) 

enum class EMovingType : uint8;
template<> SBHOMEWORK_20_5_API UEnum* StaticEnum<EMovingType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
