// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SBHOMEWORK_20_5_FoodField_generated_h
#error "FoodField.generated.h already included, missing '#pragma once' in FoodField.h"
#endif
#define SBHOMEWORK_20_5_FoodField_generated_h

#define sbHomework_20_5_Source_sbHomework_20_5_FoodField_h_14_SPARSE_DATA
#define sbHomework_20_5_Source_sbHomework_20_5_FoodField_h_14_RPC_WRAPPERS
#define sbHomework_20_5_Source_sbHomework_20_5_FoodField_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define sbHomework_20_5_Source_sbHomework_20_5_FoodField_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFoodField(); \
	friend struct Z_Construct_UClass_AFoodField_Statics; \
public: \
	DECLARE_CLASS(AFoodField, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/sbHomework_20_5"), NO_API) \
	DECLARE_SERIALIZER(AFoodField)


#define sbHomework_20_5_Source_sbHomework_20_5_FoodField_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAFoodField(); \
	friend struct Z_Construct_UClass_AFoodField_Statics; \
public: \
	DECLARE_CLASS(AFoodField, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/sbHomework_20_5"), NO_API) \
	DECLARE_SERIALIZER(AFoodField)


#define sbHomework_20_5_Source_sbHomework_20_5_FoodField_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFoodField(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFoodField) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFoodField); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFoodField); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFoodField(AFoodField&&); \
	NO_API AFoodField(const AFoodField&); \
public:


#define sbHomework_20_5_Source_sbHomework_20_5_FoodField_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFoodField(AFoodField&&); \
	NO_API AFoodField(const AFoodField&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFoodField); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFoodField); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFoodField)


#define sbHomework_20_5_Source_sbHomework_20_5_FoodField_h_14_PRIVATE_PROPERTY_OFFSET
#define sbHomework_20_5_Source_sbHomework_20_5_FoodField_h_11_PROLOG
#define sbHomework_20_5_Source_sbHomework_20_5_FoodField_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	sbHomework_20_5_Source_sbHomework_20_5_FoodField_h_14_PRIVATE_PROPERTY_OFFSET \
	sbHomework_20_5_Source_sbHomework_20_5_FoodField_h_14_SPARSE_DATA \
	sbHomework_20_5_Source_sbHomework_20_5_FoodField_h_14_RPC_WRAPPERS \
	sbHomework_20_5_Source_sbHomework_20_5_FoodField_h_14_INCLASS \
	sbHomework_20_5_Source_sbHomework_20_5_FoodField_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define sbHomework_20_5_Source_sbHomework_20_5_FoodField_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	sbHomework_20_5_Source_sbHomework_20_5_FoodField_h_14_PRIVATE_PROPERTY_OFFSET \
	sbHomework_20_5_Source_sbHomework_20_5_FoodField_h_14_SPARSE_DATA \
	sbHomework_20_5_Source_sbHomework_20_5_FoodField_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	sbHomework_20_5_Source_sbHomework_20_5_FoodField_h_14_INCLASS_NO_PURE_DECLS \
	sbHomework_20_5_Source_sbHomework_20_5_FoodField_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SBHOMEWORK_20_5_API UClass* StaticClass<class AFoodField>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID sbHomework_20_5_Source_sbHomework_20_5_FoodField_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
