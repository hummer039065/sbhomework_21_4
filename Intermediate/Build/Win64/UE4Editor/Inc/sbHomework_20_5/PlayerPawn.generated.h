// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SBHOMEWORK_20_5_PlayerPawn_generated_h
#error "PlayerPawn.generated.h already included, missing '#pragma once' in PlayerPawn.h"
#endif
#define SBHOMEWORK_20_5_PlayerPawn_generated_h

#define sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h_17_SPARSE_DATA
#define sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHandleHorizontalPlayerMove); \
	DECLARE_FUNCTION(execHandleVerticalPlayerMove);


#define sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHandleHorizontalPlayerMove); \
	DECLARE_FUNCTION(execHandleVerticalPlayerMove);


#define sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerPawn(); \
	friend struct Z_Construct_UClass_APlayerPawn_Statics; \
public: \
	DECLARE_CLASS(APlayerPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/sbHomework_20_5"), NO_API) \
	DECLARE_SERIALIZER(APlayerPawn)


#define sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerPawn(); \
	friend struct Z_Construct_UClass_APlayerPawn_Statics; \
public: \
	DECLARE_CLASS(APlayerPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/sbHomework_20_5"), NO_API) \
	DECLARE_SERIALIZER(APlayerPawn)


#define sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerPawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerPawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPawn(APlayerPawn&&); \
	NO_API APlayerPawn(const APlayerPawn&); \
public:


#define sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPawn(APlayerPawn&&); \
	NO_API APlayerPawn(const APlayerPawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayerPawn)


#define sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h_17_PRIVATE_PROPERTY_OFFSET
#define sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h_14_PROLOG
#define sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h_17_PRIVATE_PROPERTY_OFFSET \
	sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h_17_SPARSE_DATA \
	sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h_17_RPC_WRAPPERS \
	sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h_17_INCLASS \
	sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h_17_PRIVATE_PROPERTY_OFFSET \
	sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h_17_SPARSE_DATA \
	sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h_17_INCLASS_NO_PURE_DECLS \
	sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SBHOMEWORK_20_5_API UClass* StaticClass<class APlayerPawn>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID sbHomework_20_5_Source_sbHomework_20_5_PlayerPawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
