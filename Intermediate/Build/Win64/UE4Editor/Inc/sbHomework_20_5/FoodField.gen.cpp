// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "sbHomework_20_5/FoodField.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFoodField() {}
// Cross Module References
	SBHOMEWORK_20_5_API UClass* Z_Construct_UClass_AFoodField_NoRegister();
	SBHOMEWORK_20_5_API UClass* Z_Construct_UClass_AFoodField();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_sbHomework_20_5();
	SBHOMEWORK_20_5_API UClass* Z_Construct_UClass_AFood_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
// End Cross Module References
	void AFoodField::StaticRegisterNativesAFoodField()
	{
	}
	UClass* Z_Construct_UClass_AFoodField_NoRegister()
	{
		return AFoodField::StaticClass();
	}
	struct Z_Construct_UClass_AFoodField_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Food_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Food;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FoodClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_FoodClass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AFoodField_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_sbHomework_20_5,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFoodField_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FoodField.h" },
		{ "ModuleRelativePath", "FoodField.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFoodField_Statics::NewProp_Food_MetaData[] = {
		{ "ModuleRelativePath", "FoodField.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFoodField_Statics::NewProp_Food = { "Food", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFoodField, Food), Z_Construct_UClass_AFood_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFoodField_Statics::NewProp_Food_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFoodField_Statics::NewProp_Food_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFoodField_Statics::NewProp_FoodClass_MetaData[] = {
		{ "Category", "FoodField" },
		{ "ModuleRelativePath", "FoodField.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AFoodField_Statics::NewProp_FoodClass = { "FoodClass", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFoodField, FoodClass), Z_Construct_UClass_AFood_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AFoodField_Statics::NewProp_FoodClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFoodField_Statics::NewProp_FoodClass_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AFoodField_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFoodField_Statics::NewProp_Food,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFoodField_Statics::NewProp_FoodClass,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AFoodField_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AFoodField>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AFoodField_Statics::ClassParams = {
		&AFoodField::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AFoodField_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AFoodField_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AFoodField_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AFoodField_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AFoodField()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AFoodField_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AFoodField, 1294867078);
	template<> SBHOMEWORK_20_5_API UClass* StaticClass<AFoodField>()
	{
		return AFoodField::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AFoodField(Z_Construct_UClass_AFoodField, &AFoodField::StaticClass, TEXT("/Script/sbHomework_20_5"), TEXT("AFoodField"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFoodField);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
