// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "sbHomework_20_5/sbHomework_20_5GameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodesbHomework_20_5GameModeBase() {}
// Cross Module References
	SBHOMEWORK_20_5_API UClass* Z_Construct_UClass_AsbHomework_20_5GameModeBase_NoRegister();
	SBHOMEWORK_20_5_API UClass* Z_Construct_UClass_AsbHomework_20_5GameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_sbHomework_20_5();
// End Cross Module References
	void AsbHomework_20_5GameModeBase::StaticRegisterNativesAsbHomework_20_5GameModeBase()
	{
	}
	UClass* Z_Construct_UClass_AsbHomework_20_5GameModeBase_NoRegister()
	{
		return AsbHomework_20_5GameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_AsbHomework_20_5GameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AsbHomework_20_5GameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_sbHomework_20_5,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AsbHomework_20_5GameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "sbHomework_20_5GameModeBase.h" },
		{ "ModuleRelativePath", "sbHomework_20_5GameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AsbHomework_20_5GameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AsbHomework_20_5GameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AsbHomework_20_5GameModeBase_Statics::ClassParams = {
		&AsbHomework_20_5GameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AsbHomework_20_5GameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AsbHomework_20_5GameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AsbHomework_20_5GameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AsbHomework_20_5GameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AsbHomework_20_5GameModeBase, 1692510710);
	template<> SBHOMEWORK_20_5_API UClass* StaticClass<AsbHomework_20_5GameModeBase>()
	{
		return AsbHomework_20_5GameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AsbHomework_20_5GameModeBase(Z_Construct_UClass_AsbHomework_20_5GameModeBase, &AsbHomework_20_5GameModeBase::StaticClass, TEXT("/Script/sbHomework_20_5"), TEXT("AsbHomework_20_5GameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AsbHomework_20_5GameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
