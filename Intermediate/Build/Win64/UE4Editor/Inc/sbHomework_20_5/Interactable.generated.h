// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SBHOMEWORK_20_5_Interactable_generated_h
#error "Interactable.generated.h already included, missing '#pragma once' in Interactable.h"
#endif
#define SBHOMEWORK_20_5_Interactable_generated_h

#define sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_13_SPARSE_DATA
#define sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_13_RPC_WRAPPERS
#define sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SBHOMEWORK_20_5_API UInteractable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteractable) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SBHOMEWORK_20_5_API, UInteractable); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteractable); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SBHOMEWORK_20_5_API UInteractable(UInteractable&&); \
	SBHOMEWORK_20_5_API UInteractable(const UInteractable&); \
public:


#define sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SBHOMEWORK_20_5_API UInteractable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SBHOMEWORK_20_5_API UInteractable(UInteractable&&); \
	SBHOMEWORK_20_5_API UInteractable(const UInteractable&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SBHOMEWORK_20_5_API, UInteractable); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteractable); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteractable)


#define sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_13_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUInteractable(); \
	friend struct Z_Construct_UClass_UInteractable_Statics; \
public: \
	DECLARE_CLASS(UInteractable, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/sbHomework_20_5"), SBHOMEWORK_20_5_API) \
	DECLARE_SERIALIZER(UInteractable)


#define sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_13_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_13_GENERATED_UINTERFACE_BODY() \
	sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_13_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_13_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_13_GENERATED_UINTERFACE_BODY() \
	sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_13_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IInteractable() {} \
public: \
	typedef UInteractable UClassType; \
	typedef IInteractable ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_13_INCLASS_IINTERFACE \
protected: \
	virtual ~IInteractable() {} \
public: \
	typedef UInteractable UClassType; \
	typedef IInteractable ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_10_PROLOG
#define sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_13_SPARSE_DATA \
	sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_13_RPC_WRAPPERS \
	sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_13_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_13_SPARSE_DATA \
	sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	sbHomework_20_5_Source_sbHomework_20_5_Interactable_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SBHOMEWORK_20_5_API UClass* StaticClass<class UInteractable>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID sbHomework_20_5_Source_sbHomework_20_5_Interactable_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
