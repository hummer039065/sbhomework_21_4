// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SBHOMEWORK_20_5_sbHomework_20_5GameModeBase_generated_h
#error "sbHomework_20_5GameModeBase.generated.h already included, missing '#pragma once' in sbHomework_20_5GameModeBase.h"
#endif
#define SBHOMEWORK_20_5_sbHomework_20_5GameModeBase_generated_h

#define sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h_15_SPARSE_DATA
#define sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h_15_RPC_WRAPPERS
#define sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAsbHomework_20_5GameModeBase(); \
	friend struct Z_Construct_UClass_AsbHomework_20_5GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AsbHomework_20_5GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/sbHomework_20_5"), NO_API) \
	DECLARE_SERIALIZER(AsbHomework_20_5GameModeBase)


#define sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAsbHomework_20_5GameModeBase(); \
	friend struct Z_Construct_UClass_AsbHomework_20_5GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AsbHomework_20_5GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/sbHomework_20_5"), NO_API) \
	DECLARE_SERIALIZER(AsbHomework_20_5GameModeBase)


#define sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AsbHomework_20_5GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AsbHomework_20_5GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AsbHomework_20_5GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AsbHomework_20_5GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AsbHomework_20_5GameModeBase(AsbHomework_20_5GameModeBase&&); \
	NO_API AsbHomework_20_5GameModeBase(const AsbHomework_20_5GameModeBase&); \
public:


#define sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AsbHomework_20_5GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AsbHomework_20_5GameModeBase(AsbHomework_20_5GameModeBase&&); \
	NO_API AsbHomework_20_5GameModeBase(const AsbHomework_20_5GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AsbHomework_20_5GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AsbHomework_20_5GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AsbHomework_20_5GameModeBase)


#define sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h_12_PROLOG
#define sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h_15_SPARSE_DATA \
	sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h_15_RPC_WRAPPERS \
	sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h_15_INCLASS \
	sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h_15_SPARSE_DATA \
	sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SBHOMEWORK_20_5_API UClass* StaticClass<class AsbHomework_20_5GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID sbHomework_20_5_Source_sbHomework_20_5_sbHomework_20_5GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
